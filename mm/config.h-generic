/*
 * config.h-generic - GENERIC configuration file, for doc purposes.
 *
 *	ALL of the following items must be either left undefined, or
 *	set to a site-specific value, by each installation's config.h file.
 */

/* Define USAGE if you want MM to log usage statistics.
 * If so, you can also define USAGEFILE if the default location
 * of the log file should be changed.
 */
/* #define USAGE */		/* Log usage statistics to USAGEFILE */
/* #define USAGEFILE "foo" */	/* Defaulted in pathnames.h */

/*
 * The HOSTNAME macro is used only as a last resort, if no other way
 * of determining the hostname will work.  If you have a domain name,
 * i.e. there are dots in the official name of your host, include only
 * the first component of the name.
 */
/* #define HOSTNAME "ourhost"		/* local hostname */

/*
 * If you have an official domain name for your host, and the domain name
 * is not discernible with gethostbyname(3), define LOCALDOMAIN
 * to be the name of your domain, preceded by a "."; otherwise, define
 * LOCALDOMAIN to be a null string.  For example, if the name of your host
 * is podunk.edu, you should specify:
 *	#define LOCALDOMAIN ".edu"
 * If your host is vax.cs.podunk.edu, you'd use
 *	#define LOCALDOMAIN ".cs.podunk.edu"
 */
/* #define LOCALDOMAIN ".ourdomain.edu"	/* local domain */

/*
 * If your site likes to make outgoing mail appear as if it all comes
 * from one central host, define HIDDENNET as the name of that host.
 * Otherwise, leave HIDDENNET undefined.
 */
/* #define HIDDENNET "mailhost.ourdomain.edu"	/* fake mail host name */

/*
 * If your mailer supports .forward files, and you want MM to display
 * the contents of .forward file (you may not wish to do this if it's
 * likely that a user's .forward file will be ignored because of conflicting
 * entries in /usr/lib/aliases), define FORWARD_FILE as the name of the
 * file in the user's home directory.
 */
#define FORWARD_FILE ".forward"		/* we use .forward files */

/*
 * If you have GNU Emacs, define GNUEMACS to be the name with which it
 * is usually referred by your users.  If GNUEMACS is not defined, special
 * code used to interface MM with GNU Emacs will not be compiled into MM.
 */
#define GNUEMACS "emacs"		/* we have GNU Emacs */

/*
 * If you have, and use, the Sun Yellow Pages facility, define
 * HAVE_YP.
 */
/* #define HAVE_YP */				/* we use yp */

/*
 * If you have NFS and users will be referencing mail files on remote
 * filesystems, define HAVE_NFS.
 */
#define HAVE_NFS			/* we use NFS */

/*
 * If you have support for disk quotas, and you use them, define QUOTAS
 * Otherwise leave it undefined.
 */
#undef HAVE_QUOTAS			/* we have quota support and use it */
