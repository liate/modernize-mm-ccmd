/*
 * Copyright (c) 1986, 1990 by The Trustees of Columbia University in
 * the City of New York.  Permission is granted to any individual or
 * institution to use, copy, or redistribute this software so long as it
 * is not sold for profit, provided this copyright notice is retained.
 */

#ifdef RCSID
#ifndef lint
static char *compat_rcsid = "$Header: /f/src2/encore.bin/cucca/mm/tarring-it-up/RCS/compat.h,v 2.2 90/10/04 18:23:48 melissa Exp $";
#endif
#endif /* RCSID */

/*
 * This file contains definitions needed to resolve incompatibilities
 * between various different C library implementations.  Conditional
 * declarations here depend on macros defined in config.h, and perhaps
 * <sys/param.h> (included by osfiles.h), so this file should be included
 * after them, but before any other other header files or source code
 * which may depend on these declarations.
 */

/*
 * map index & rindex to strchr and strrchr if we don't have them.
 */
#ifndef HAVE_INDEX
#define index strchr
#define rindex strrchr
#endif

/*
 * non-Berkeley C libaries generally don't have bzero and friends
 */
#ifndef HAVE_BSTRING
#define bzero(a,b)	memset((a),0,b)
#define bcopy(a,b,c)	memcpy((b),(a),c)
#define bcmp(a,b,c)	memcmp((a),(b),c)
#endif

/*
 * Some systems don't have rename(2)
 */
#ifndef HAVE_RENAME
#define rename bsdrename
#endif

/*
 * Some systems don't have have vfork(2)
 */
#ifndef HAVE_VFORK
#define vfork fork
#endif

/*
 * Non-berkeley systems generally don't have getwd(2).
 */
#ifndef HAVE_GETWD
#define getwd bsdgetwd
#endif

/*
 * In SVR3, signal(2) is declared void (*signal())() rather than
 * int (*signal())().
 */
#ifdef HAVE_VOIDSIG
#define signalhandler void
#else
#define signalhandler int
#endif

/*
 * Some older systems systems don't have unistd.h.
 */
#if defined(L_SET) && !defined(SEEK_SET)
#define SEEK_SET	L_SET
#define SEEK_CUR	L_INCR
#define SEEK_END	L_XTND
#endif

/*
 * Make sure filename strings are long enough.
 */
#ifndef MAXPATHLEN
#define MAXPATHLEN 1024
#endif

/*
 * Define a macro to give a process its own process group.
 */
#ifdef HAVE_SETPGID	/* New standard */
# define new_process_group() setpgid(0, getpid ())
#else
# ifdef HAVE_BSD_SETPGRP
#  define new_process_group() setpgrp(0, getpid ())
# else
#  define new_process_group() setpgrp()
# endif
#endif

/*
 * Some systems define both, so the distinction isn't useful, and
 * we'll use SIGCHLD internally.
 */
#if defined(SIGCLD) && !defined(SIGCHLD)
#define SIGCHLD SIGCLD
#endif

/*
 * On systems without the Berkeley signal behavior, use special
 * versions of read(2) and write(2)
 */
#if defined(HAVE_SIGSETS)
#  define mmsigset_t sigset_t
#  define mmsigemptyset(set) sigemptyset(set)
#  define mmsigfillset(set)  sigfillset(set)
#  define mmsigaddset(set,s) sigaddset(set,s)
#  define mmsigdelset(set,s) sigdelset(set,s)
#  define mmsigismember(set,s) sigismember(set,s)
#else
#  if !defined(HAVE_BSD_SIGNALS) && !defined(HAVE_POSIX_SIGNALS)
	/* On systems without BSD or better signal behavior, use special
	 * versions of read(2) and write(2), plus BSD signal masks
	 */
#    define read sys_read
#    define write sys_write
#  endif
#  ifndef sigmask
#    define sigmask(m) (1 << ((m) - 1))
#  endif
#  define mmsigset_t unsigned long
#  define mmsigemptyset(set) (*(set) = 0)
#  define mmsigfillset(set)  (*(set) = ~0L)
#  define mmsigaddset(set,s) (*(set) |= sigmask(s))
#  define mmsigdelset(set,s) (*(set) &= ~sigmask(s))
#  define mmsigismember(set,s) (*(set) & sigmask(s))
#endif
signalhandler *mmsignal();	/* Replacement for signal()  */


/*
 * Finally, try to ensure that one of BSD or SYSV is #defined, for
 * the benefit of code which assumes that one or the other is true.
 */

#if !defined(MM_OS_BSD) && !defined(MM_OS_SYSV)
#   if hpux || u3b || u3b2 || u3b5 || u3b15 || u3b20 || SVR2 || SVR3 || AIX
#	define MM_OS_SYSV 1
#   elif defined(TIOCNOTTY) || sun || ultrix || accel || pyr || bsd4_2
#	define MM_OS_BSD 1
#   endif
#endif
