BROWSE  -  browse through the current message sequence

Usage:  BROWSE

If you have used the READ command at the MM> prompt to start reading a series
  of messages, you can use the BROWSE command at the Read> prompt to switch
  to the "browse mode" type of screen display and commands.
  
For each message, it shows a display like a HEADERS command, with the prompt
  "... More?--".  Type one of these options:
    SPACE  read the message
      n	   go to the next message
      p	   go to the previous message
      q    quit browsing
      s	   switch direction of browse
      c	   copy the message to a file  (COPY command)
      r	   reply to the message  (REPLY command)
      d	   delete the message  (DELETE command)
      f    flag the message  (FLAG command)
      k	   add a keyword to the message  (KEYWORD command)
      ?	   display a list of commands similar to this one
  
You can use the BROWSE command from the MM> prompt to read messages in
  "browse mode".  The BROWSE command at the Read> prompt exists to let you
  change your mind after you start reading with the READ command.

For more help type "help" and one of these topics:
  copy  reply  delete  flag  keyword     
  headers   copy   reply   delete   flag   keyword
  top-level browse   top-level read
