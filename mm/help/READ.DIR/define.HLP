DEFINE  -  define a mail-alias for an address

Usage:  DEFINE mail-alias [address]

You can DEFINE a personal mail-alias, usually to save repeated typing of a 
  long address or list of addresses.  You can then use the mail-alias in the
  TO and CC fields of your messages.  To save the mail-alias permanently, 
  you must also use the command SAVE-INIT before you leave MM.

For mailing lists, see the use of "@@filename", on the next screen.

Examples:
    Read>define rich xyz@comp.bfu.edu        #a long address
    Read>define mygroup abc,jf3@cunixa,grr   #a list of three people
    Read>define pals @@palslist              #a file of userids
    Read>define ?                            #list existing aliases   
    Read>define rich			     #undefine rich

To change the address(es) of a mail-alias, DEFINE it again.  If you omit
  the address, the mail-alias is undefined.
  
For mailing lists, use @ or @@ and a filename, meaning as follows:
  
  @filename	--Copy addresses from a file when MM starts up.
  @@filename	--Copy addresses from a file when the mail-alias is used.  

  The file must contain one or more addresses separated by commas; it may 
  have any number of lines.  The "@@" method is generally preferable,
  because you may want to change the address file while the MM process is
  running, and use the changed version.

  You do not need to use DEFINE to use a mailing list; you could just 
  use @filename at the TO or CC prompt.

For more help type "help" and one of these topics:
   save-init   show   set   who   send   set mail-alias
   addressing   customization
