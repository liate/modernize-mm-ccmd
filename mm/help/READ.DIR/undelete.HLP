UNDELETE  -  remove the mark for deletion on the current message

Usage:  UNDELETE

The UNDELETE command removes the D indicator from the header line of the
  current message.  The D shows that you marked the message for deletion with 
  the DELETE or MOVE command.  If you change your mind, you can UNDELETE 
  the message.

When you use the EXPUNGE, EXIT or BYE commands, messages marked for deletion
  will be permanently removed.  You cannot undo that.  

Example: You find a message was marked deleted and decide to keep it
    MM>read from howie              #You want to see a certain message
     Message 80 deleted, ignored    #You realize this one is deleted
    Read>type                       #Displays the deleted message, 80
     ..message 80 is shown..        #You read it, and decide to keep it
    Read>undelete                   #Undeletes message 80

For more help type 'help' and one of these topics:
  delete   move   type   message-handling
