KEYWORD  -  group related messages by assigning keywords

Usage:  KEYWORD keyword-name

The KEYWORD command allows you to group together related messages in
  your mail file using user-defined keywords.  A message may have more
  than one keyword assigned to it.  Any message that has at least
  one keyword is marked by the "K" field on the header line.  

The keywords will appear in a header field, "Keywords", when you read the
  messages.  You can use the keyword as a message-sequence; see the 
  example below.

Examples:
    Read>keyword ?	      #shows all keywords defined or in use
    Read>keyword bird         #assigns the keyword "bird" to the message
    MM>headers keyword bird   #shows headers of messages with keyword "bird"

For more help type "help" and one of these topics:
  message-tagging   unkeyword   set keywords   message-sequence   headers
