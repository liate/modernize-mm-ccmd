TYPE  -  display the current message again

Usage:  TYPE
  
The TYPE command simply displays the current message.  Doing so may be 
  useful when a long message has scrolled off the top of the screen.

In READ MODE, TYPE will display messages marked for deletion.  So:
    MM>read 80
     Message 80 deleted, ignored.   #message 80 is not shown, but is current
    Read>type                       #displays message 80

If you have used SET DONT-TYPE-HEADERS or SET ONLY-TYPE-HEADERS
  to restrict what header fields normally appear, the prefix
  LITERAL will override those settings and display all the header
  fields anyway.
		
For more help type "help" and one of these topics:
  literal   set dont-type-headers     set only-type-headers
