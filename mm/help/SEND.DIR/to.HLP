TO  -  add an address to the TO field

Usage:  TO address

The TO field contains the main address(es) of your message.  (There are
  various "carbon copy" fields for secondary copies: CC, BCC, FCC.)

The TO command at the Send> prompt allows you to add more addresses
  to the TO field.  (If the TO field has been erased, the TO command will 
  create it.)  The command DISPLAY TO will show you the current content of 
  the TO field, and ERASE TO will erase the entire field; REMOVE will
  remove individual addresses.

Example: Send>display to
         To: brennan
         Send>to alan
         Send>display to
         To: brennan, alan

For more help, type "help" and one of these topics:
  display   cc   bcc   erase   remove   addressing   header-field
