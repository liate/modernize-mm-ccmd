CD  -  change directory

Usage:  cd [directory-name]

CD is like the shell command cd.  It changes the working directory for the
  MM process, not for the shell that invoked MM.  If you are using commands
  that default to the working directory (like LIST, TAKE, the FCC header,
  inserting a file with control-b), CD can simplify things.

If no directory-name is given, CD changes to your home directory.  As a
  directory-name, you may specify a full path starting with root (/), 
  or a relative path from the current working directory (the one you are 
  in when you use the CD command).  

Examples:
    Send>cd                             #to home directory
    Send>cd oldmail                     #relative path
    Send>cd /f/us/us/lafarge/oldmail    #full path (absolute path)

For more help type "help" and one of these topics:
  pwd   list   take   other   set mail-directory   directory
