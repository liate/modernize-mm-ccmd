BCC  -  add an address to the BCC field

Usage:  BCC address 

The BCC field is for "blind carbon copy".  BCC is like CC, but the BCC
  field is removed during mailing, so readers of your message do not
  see it, and therefore do not know about the BCC copies.  Use of the 
  field is optional.
  
The BCC command at the Send> prompt allows you to add more addresses
  to the BCC field.  If the BCC field does not exist, the BCC command will 
  create it.  The command DISPLAY BCC will show you the current content of
  the BCC field, and ERASE BCC will erase the entire field; REMOVE will
  remove individual addresses.

Example: Send>bcc walter
         Send>display bcc
         Bcc: walter

For more help, type "help" and one of these topics:
  display   to   cc   fcc   erase   remove   addressing   header-field
  set default-bcc-list   set prompt-for-bcc
