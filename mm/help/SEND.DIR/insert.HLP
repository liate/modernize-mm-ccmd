INSERT  -  insert a file in the outgoing message

Usage:  INSERT filename

The INSERT command inserts a file after the text of the message.  The DISPLAY
  command will then show the entire message with the inserted text.

You can do the same thing while you are still typing in your text.
  In MM's text mode, type control-b, and then at the prompt "Filename?",
  type the name of the file.  In emacs, type "ESC-x insert-file" and
  the filename.  An advantage of these two methods is that it is simple
  then to type more text after the inserted file, if you want to.  

Example:
    Send>insert chapter1	#inserts your file "chapter1" following
				#your message.

For more help, type "help" and one of these topics:
  display    edit   basic   text-mode   top-level-mode send
