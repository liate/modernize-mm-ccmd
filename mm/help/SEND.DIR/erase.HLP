ERASE  -  remove (part of) the outgoing message

Usage:  ERASE [field]

The ERASE command removes parts of, or all of, the outgoing message.  Use
  ERASE and then the commands BCC, CC, FCC, and TO to erase and replace
  the contents of address header fields.  (The commands SUBJECT and REPLY-TO
  overwrite the old contents, so ERASE is not necessary for replacement.)

By itself, ERASE erases "all"; you may also erase part of the message by
  specifying "header", "text", or the name of an individual header-field.

Examples:
    Send>erase			#erases the entire message (erase all)
    Send>erase header		#erases all header fields
    Send>erase text		#erases the text
    Send>erase to		#erases the TO field
    Send>erase cc		#erases the CC field
  Also: erase subject, erase fcc, erase reply-to, etc.

For more help, type "help" and one of these topics:
  remove   send   user-header   header-field
