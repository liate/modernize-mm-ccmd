LIST  -  send the current message to a file or to a shell command

Usage:  LIST [/switch] filename  
   or:  LIST [/switch]  |"shell-command"

Please note that LIST acts on the "current message" in your mail file,
  and not on the outgoing message.  If you are REPLYing, the current
  message is the one you are replying to.  To copy the outgoing message
  to a file, use SAVE-DRAFT or FCC.

The LIST command copies the message to a file or to a shell command.
  The result contains a table of contents (like a HEADERS command) for
  the message, then a page break, then the message.  You may specify
  as an option the switch /headers-only, to print only the header 
  (like a HEADERS command).

The command LITERAL LIST will print all header fields regardless of 
  settings of DONT-PRINT-HEADERS or ONLY-PRINT-HEADERS.  SET LIST-INCLUDE-
  HEADERS NO will eliminate the table of contents on the first page of output.
  
The file created with LIST is not a mail file readable by MM (use COPY or
  MOVE to create a mail file).  LIST does not append to existing files,
  so if you use the name of an existing file you will be replacing it.

The first version of the LIST command sends the current message to a file.  

    Send>list joe 			#sends the message to a file "joe"

The second version of the LIST command is similar, but sends the current
  message as input to a shell command.  For example, one way of printing
  a message is to LIST it to a print command.

    Send>list | "print" 	   #sends the message to the
                                    shell command "print"

For more help, type "help" and one of these topics:
  literal   print   copy   move   save-draft   fcc   other
  set dont-print-headers   set only-print-headers   set list-include-headers
