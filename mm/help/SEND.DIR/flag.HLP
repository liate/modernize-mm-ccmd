FLAG  -  mark the current message so it stands out

Usage:  FLAG 

Please note that FLAG acts on the "current message" in your mail file,
  and not on the outgoing message.  If you are REPLYing, the current
  message is the one you are replying to.

The FLAG command puts a "flag" or marker on the message.  The letter "F"
  appears next to it in the header line.  By default, when you start
  up MM you see headers of flagged messages as well as new ones.  You
  can also use "flagged" as a message-sequence.

Examples:
    Send>flag 		#flags the current message
    MM>read flagged	#reads flagged messages

The UNFLAG command will remove the flag.

For more help type "help" and one of these topics:
  message-tagging   unflag   headers   set display-flagged-messages
