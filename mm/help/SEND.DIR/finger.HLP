FINGER  -  get information about a user

Usage:  FINGER [-options] [string] [output]

The FINGER command is defined by SET FINGER-COMMAND; the default is the
  system command "finger".  FINGER gets information about other users on
  the system.  Specify options and/or a string (userid) and/or an output
  direction (like "|more") the same as in the shell.  

For more information on the shell command, type "man finger" at the shell
  prompt, or "!man finger" at any of the MM prompts.

Examples:
    Send>finger		   #shows all users logged in now
    Send>finger | more 	   #same as above, one screen at a time
    Send>finger -Q string  #shows all users with names beginning "string"
    Send>finger userid     #shows information about one user
    Send>finger -v userid  #shows more information about one user

For more help, type "help" and one of these topics:
  who   set finger-command   shell
