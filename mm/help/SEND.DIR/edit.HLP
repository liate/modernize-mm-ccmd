EDIT  -  edit the outgoing message

Usage:  EDIT [field]

The EDIT command brings you into an editor, so you can work on the outgoing
  message.  The editor used is defined with SET EDITOR; the default is emacs.

You can also start the editor by typing control-e when you are typing text
  (or as soon as you reach the "Message..." prompt in the SEND procedure).
  The EDIT command is usually used to go back and make further changes 
  after you have DISPLAYed the message or used RESTORE-DRAFT.

EDIT normally brings the entire message into the editor; with emacs, the 
  header and text are normally in separate buffers (see SET GNUEMACS-MMAIL).  
  You can also specify "header" or "text".

Using an editor is useful if you are composing a reply with the REPLY
  INCLUDING command, since you can move back into the included text
  and add comments within it.  

For more help, type "help" and one of these topics: 
    set editor   set use-editor-always   set gnuemacs-mmail   text   other
