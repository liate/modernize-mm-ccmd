REPLY-TO  -  create (replace) the REPLY-TO field

Usage:  REPLY-TO [address]

The REPLY-TO command creates a field called REPLY-TO in the outgoing
  message.  Replies normally go to the address in the FROM field, but
  if there is a REPLY-TO field, replies go there instead.  If there 
  already is a REPLY-TO field, the REPLY-TO command replaces it; the
  REPLY-TO command with no address eliminates the field.

Consider changing the REPLY-TO and FROM fields if you are sending a 
  message for someone else who should get the REPLY messages.  

Examples:
  If your id is "abc1", you could do the following:
    Send>reply-to abccu@cuvmb	#replies go to your other id, abccu@cuvmb
    Send>reply-to abc1, def2	#replies go to both you and def2

For more help, type "help" and one of these topics:
  display   from   in-reply-to   send   addressing   header-field   
  set default-reply-to
