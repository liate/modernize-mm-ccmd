EXIT  -  leave MM; suspend the process and expunge deleted messages

Usage:  EXIT

The EXIT command expunges deleted messages, rewrites the current mail
  file if it was modified, suspends the MM process, and returns you to
  the shell.  EXIT therefore is the same as EXPUNGE and QUIT combined.

Deleted messages are those you mark for deletion with the DELETE or
  MOVE commands. They are still in the mail file until you remove
  them permanently with the EXPUNGE or EXIT commands.

The variable SUSPEND-ON-EXIT controls whether EXIT suspends MM.

There are three ways to leave MM.  The defaults are:
  EXIT	suspends MM	expunges deleted messages
  QUIT	suspends MM	does not expunge
  BYE	kills MM	asks whether to expunge

For more help type "help" and one of these topics:
  expunge   quit   bye   delete   move   basic   set suspend-on-exit
