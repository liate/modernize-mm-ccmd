TYPE  -  display messages, without entering READ mode.

Usage:  TYPE [message-sequence]
  Unless specified, the message-sequence is "current".

The TYPE command simply displays messages.  The READ command also displays 
  messages, but after READing each message, you are in READ mode.

The command LITERAL TYPE will override any settings of DONT-TYPE-HEADERS
  or ONLY-TYPE-HEADERS and display all header fields.

Examples:
    MM>type			#types the current message
    MM>type 4:9			#types messages 4 to 9
    MM>type subj meeting	#types messages with "meeting" in subject
		
For more help type "help" and one of these topics:
  read   message-sequence   literal   
  set dont-type-headers     set only-type-headers
