READ  -  read messages

Usage:  READ [message-sequence]
  Unless specified, the message-sequence is "unseen".

The READ command is used to read a message or set of messages.  After each
  message is displayed, you will be in READ mode (Read> prompt), at which 
  point you can take action on the current message, like DELETE, REPLY, 
  FORWARD, and so on.  Compare to TYPE, which only displays messages.

Examples:
    MM>read			#reads unseen messages
    MM>read 4:9			#reads messages 4 to 9
    MM>read subj meeting	#reads messages with "meeting" in subject

Good use of message-sequences gets useful results, such as:
    MM>read from walter after 4/1/90 subj mail
  You could use HEADERS first, and then READ PREV (previous sequence).

For more help type "help" and one of these topics:
  review   type   headers   message-sequence   basic   read-mode help
  set default-read-command   set dont-type-headers   set only-type-headers
