EXAMINE  -  go to another mail file, read-only

Usage: EXAMINE filename
  
The EXAMINE command brings you to another mail file in read-only mode.  While
  you can read the mail file and possibly modify it, nothing you do will
  change the file on disk.  EXAMINE is useful for looking at an archive file
  that should not be changed, or someone else's file.  New mail will not be
  put into your main mail file (mbox) while you EXAMINE it.

The variable MODIFY-READ-ONLY controls whether you can modify a read-
  only file internally.  Normally, it is set to YES, but the modifications 
  are not saved to disk.  

Example:
    MM>examine archive     #gives read-only access to the file "archive"

Use GET with a filename for read/write access.  After using EXAMINE, use GET
  with no filename to have read/write access to mbox.

For more help type "help" and one of these topics:
  get   move   copy   write   filing   set modify-read-only
