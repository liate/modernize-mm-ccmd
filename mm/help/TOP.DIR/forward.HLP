FORWARD  -  forward messages to another address

Usage:  FORWARD [message-sequence]
  Unless specified, the message-sequence is "current".

  At the prompt "To", type the address to which to forward the message(s).
  At the prompt "Message...", you may type a message, which will appear 
  above the forwarded message(s).  End with ESCAPE, as usual.

The FORWARD command is used to forward a message or set of messages you 
  have received to some other address, with, optionally, comments from you 
  at the beginning. If more than one message is specified, they will all 
  be sent together as one forwarded message with a header listing the 
  contents.  You will appear as the sender, on the FROM line, of the 
  forwarded mail, and therefore replies go to you.  After you have typed 
  your message (if any) and press ESCAPE, you will be in SEND MODE.

If you use the REMAIL command instead: you cannot insert comments; each
  message is remailed as a separate message; and replies go to the original 
  sender.  FORWARD is like making a photocopy to show someone; REMAIL is 
  like correcting the address on an envelope.
  
There is an ethical problem involved with FORWARD or REMAIL.  Consider
  whether the original sender would want the message seen by someone else.
  If you use FORWARD, you can use the EDIT command at the Send> prompt to
  edit the forwarded text to delete sensitive portions.

For more help, type "help" and one of these topics:
  send   remail   addressing   message-sequence   message-handling   text-mode
  send-mode edit
