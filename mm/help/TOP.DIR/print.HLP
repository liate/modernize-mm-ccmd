PRINT  -  print messages

Usage:  PRINT [/switch] [message-sequence]
  Unless specified, the message-sequence is "current".

The PRINT command sends the messages to a printer, as determined by the 
  MM variable PRINT-FILTER.  By default, it uses /usr/local/bin/print, 
  which asks you what printer you want (and offers a list) unless you have
  a printer defined in your .profile file.  Students can use CUCCA's
  printers in the Int Affairs and Mudd micro labs (etc); see the lab
  consultant for printer names and capabilities.

If you are using Kermit 95 or MS-DOS Kermit and have a printer directly 
  attached to your PC, you can use a print command called pcprint.  Type
  "help set print-filter" to see how to customize printing.

The optional switch "/separate-pages" puts a page break between messages,
  if you are PRINTing more than one.  The prefix LITERAL will force
  printing of all header fields regardless of any settings of SET DONT-
  PRINT-HEADERS and SET ONLY-PRINT-HEADERS.
  
Examples:
    MM>print 2:6		  #print messages 2 through 6
    MM>print /separate-pages 2:6  #put page breaks between messages
    MM>literal print 2:6          #print the message with all header fields

PRINT is somewhat similar to LIST.  For example, you can print with the
  LIST command by specifying a shell print command.  The following...
      MM>list |"lpr -Piab1" 15  
  ...uses the lpr command to print message 15 to the printer "iab1".  
  You can also write your own program accepting standard input, and 
  SET PRINT-FILTER to its name.  

For more help, type "help" and one of these topics:
  message-sequence   literal   list   message-handling
  set print-filter   set dont-print-headers   set only-print-headers  
