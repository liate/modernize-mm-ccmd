HEADERS  -  display headers (one-line summaries) of messages

Usage:  HEADERS [message-sequence]
  Unless specified, the message-sequence is "current".

The HEADERS command shows you what messages you have, with a one-line 
  display for each showing the sequence number, date, sender, subject, 
  status and message length.

Use of HEADERS with an appropriate message-sequence can help you see what
  mail you have that meets any description provided in the message-sequence.  
  You can then use the message-sequence "previous-sequence" (abbreviate as
  "p") with the next command to repeat the same message-sequence.

Examples:
    MM>headers 2:4		#messages 2 to 4
    MM>headers subj meeting	#messages with "meeting" in the subject
    MM>headers since tuesday    #messages sent on or after last Tuesday
    MM>h from chris		#messages from "chris"
    MM>h text librar		#messages with "librar" in the text
    MM>h a          		#all messages (same as "headers all") 
  
The command "headers 2:4" gives a result like this:

    K  2) 23-Dec Melissa         MM improvements (1061 chars)
       3)  6-Feb "Fuat C. Baran" address problems (749 chars)
 FA    4) 21-Feb Howie Kaye      journal article (277 chars)

The letters at the left indicate the following:
A:answered (replied to).  D:deleted.  F:flagged.  K:contains a keyword.
N:new (unseen and recent).  R:recent (arrived during this MM session).  
U:unseen (never read).

For more help type "help" and one of these topics:
  message-sequence   basic   reply   delete   flag   keyword   
  read   check
