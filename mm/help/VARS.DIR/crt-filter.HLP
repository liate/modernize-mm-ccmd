SET CRT-FILTER [program]  -   define a screen pager
Usually:       /usr/ucb/more.mm -x
     or:       /usr/ucb/more -d

The CRT-FILTER variable defines what screen pager program MM should use to
  keep long messages from scrolling off the screen.  It also controls other
  command output, like HELP screens and HEADER displays.

more -d is the "more" program with option -d.  When the screen is full, if
  there is any more, the following prompt appears:  --more--(n%) [Press
  space to continue, 'q' to quit.] .  

more.mm -x is the same as more -d, except that at the end of the message, it
  pauses and displays: [Press any key to continue] .  New users are usually
  given more.mm -x, and for consistency USE-CRT-FILTER-ALWAYS is set to YES
  so that the same prompt is displayed even for short messages

To unset it, type SET CRT-FILTER with nothing following.

For more help, type "help" and one of the following:
(at any prompt:)  save-init   set   show   set use-crt-filter-always
                  set user-level
