SET USER-HEADERS [words]  -  define CCMD completions for USER-HEADER command

The USER-HEADERS variable defines one or more words to be used to complete
  a USER-HEADER command with CCMD. Command completion is the CCMD feature 
  that lets you use the TAB key to fill in the rest of a partially typed 
  word or command.

To unset it, type SET USER-HEADERS with nothing following.

Example:
    MM>set user-headers deadline
  Then you can type:
    Send>user-header de[TAB]
  and CCMD will find "deadline" in your USER-HEADERS variable and fill in:  
    Send>user-header deadline

For more help, type "help" and one of the following:
(at any prompt:)  save-init   set   show   set header-options-file   ccmd
(at the Send> prompt:)  user-header
