SET DEFAULT-MAIL-TYPE MBOX/mtxt/babyl  -  define format for new mail files

The DEFAULT-MAIL-TYPE variable defines the file format MM will use when 
  it writes messages into a new or empty file.  Files created by WRITE, 
  COPY and MOVE are affected, as well as the main mail file if it becomes
  empty.  The usual type, "mbox", is also used by the Unix "mail" program.  
  (Because the usual format is mbox, the main mail file itself is usually 
  named "mbox", but that is not required.)

You might change this setting if you want to save mail so that you can 
  read it with another electronic mail program that requires another
  format.  The emacs RMAIL uses type "babyl".  Using more than one mail 
  program is not recommended unless you know what you are doing.

This variable is used only for new or empty files.  If the file already
  exists, MM uses the format it has (mbox, babyl, mtxt).

For more help, type "help" and one of the following:
(at any prompt:)  save-init   set   show   copy   move   write
                  set mail-file
