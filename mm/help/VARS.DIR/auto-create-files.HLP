SET AUTO-CREATE-FILES ASK/always/never  -  whether MM should create new files

With the usual ASK setting, whenever any MM command tries to write to a mail
  file that does not already exist, it will ask whether to create the file. 
  The purpose is to help you if you mistyped the name of an existing file.

If you change it to ALWAYS (or YES), MM will create new mail files 
  without asking.

If you change it to NEVER (or NO), MM will never create a new file.  This
  option probably is undesirable.  If you want to prevent accidental 
  creation of new files, ASK is better because you have a choice.

For more help, type "help" and one of these topics:
(at any prompt:)  save-init   move    copy
(at the MM> prompt:)  get
