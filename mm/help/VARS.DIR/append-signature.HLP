SET APPEND-SIGNATURE NEVER/always/ask  -  whether to use a .signature file

With the usual NEVER setting, MM does not append a .signature file to the
  end of outgoing mail.

You may create a file in your home directory, using an editor like emacs, 
  with the special filename .signature (note the dot at the beginning of 
  the name).  If you change APPEND-SIGNATURE to ALWAYS, the contents of 
  .signature will be added to the end of all mail you send out.  The 
  .signature file usually contains information like your name, organization 
  and title, e-mail and paper mail address, and so on; it is particularly
  useful when you send mail out over networks to distant points.  Please
  keep the file short; the standard rule is a maximum of 4 lines.

If you set APPEND-SIGNATURE to ASK, MM will ask you each time you send
  mail whether to use the .signature file.

For more help, type "help" and one of the following:
(at any prompt:)  save-init   set   show
