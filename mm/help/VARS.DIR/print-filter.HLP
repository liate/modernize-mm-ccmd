SET PRINT-FILTER [program]  -  define a program for the PRINT command
Usually:         /usr/local/bin/print

The PRINT-FILTER variable defines the program to use to print messages
  using the PRINT command.  

The program /usr/local/bin/print is also used when you type "print" at the 
  shell prompt (or use the LIST command with "|print").  It uses the printer 
  defined by your PRINTER environment variable, if any, and otherwise it 
  prompts you to name one of CUCCA's printers.  Type "man print" at the
  shell prompt.  See also "man lpr" and "man pr".

If you change it to /usr/local/bin/pcprint, you may be able to print at
  your own printer.  pcprint can be used with Kermit 95, C-Kermit, or
  MS-DOS Kermit or later running on a desktop computer that has access to
  a printer; or on any PC or terminal with locally attached printer that
  supports the ANSI printer control escape sequences.  Type "more
  /usr/local/bin/pcprint" at the shell prompt.

For more help, type "help" and one of the following:
(at any prompt:)  save-init   set   show   print   list
