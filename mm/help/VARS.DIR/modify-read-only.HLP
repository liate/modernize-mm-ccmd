SET MODIFY-READ-ONLY ALWAYS/never/ask  -  whether to modify read-only files

With the usual ALWAYS (or YES) setting, when you access a file read-only, 
  you can use MM commands that modify the file.  You have read-only access
  when you use EXAMINE (rather than GET), or when the file itself has
  read-only file protections.  

Many MM commands modify the file in some way, if only to mark a message 
  as read, answered or deleted.  The modifications are then normally written 
  to the file on disk at various points, but if you have read-only access, 
  the file on disk cannot be changed, so the modified version of the file is 
  thrown away when you GET or EXAMINE another file, or quit MM.

If you change it to NO, you cannot modify the file.  When you use a command 
  that would normally modify the file, you get a warning.  The NO setting
  does have the advantage of making it clear that you have read-only access.  
  Setting it to ASK is difficult to use, since MM will ask every time 
  whether to modify.

For more help, type "help" and one of the following:
(at any prompt:)  save-init   set   show
(at the MM> prompt:)  examine
