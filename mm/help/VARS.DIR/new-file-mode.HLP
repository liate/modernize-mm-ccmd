SET NEW-FILE-MODE [number]  -  define the file mode for new files
Usually:          600

The NEW-FILE-MODE variable defines the file mode, as an octal number
  (absolute value), for new files created by MM.  Commands such as COPY 
  and MOVE, LIST, and WRITE, among others, may create new files.  

The usual mode, 600, gives a high level of protection, read/write by owner
  (you) only.  This mode is shown symbolically as -rw------- when the
  file is listed by the "ls -l" command.

For information on file modes, including use of octal values, type 
  "!man chmod" at any MM prompt, or "man chmod" at the shell prompt.

For more help, type "help" and one of the following:
(at any prompt:)  save-init   set   show
