SET DEFAULT-BCC-LIST [address]  -  define a BCC address for all mail

If any address is defined, it will appear in the BCC field in all messages
  you send.  This is equivalent to using the BCC command every time and
  filling in the same address every time.  BCC is a "blind carbon copy",
  a CC field that is removed before mailing, so readers don't see it.

The address may be one address, or a list of addresses separated by commas,
  following the same rules as the "To" field.  (See "help send" at Top-
  Level or Read Mode, or "help to" at Send Mode.)  You might set DEFAULT-
  BCC-LIST to . (period, meaning yourself) to get copies of all your mail.

To unset, type SET DEFAULT-BCC-LIST with nothing following.

For more help, type "help" and one of the following:
(at any prompt:)  save-init   set   show   set default-cc-list
(at the Send> prompt:)   bcc   to
