SET AUTOWRAP-COLUMN column  -  define where to wrap lines (in text mode)
  Usually:          -7

The AUTOWRAP-COLUMN variable defines where lines wrap when you are typing
  in a message.  Wrapping is the feature that automatically starts a new 
  line of text when the current line is full (called auto-fill in emacs).

You can use a positive number to specify columns counted from the left,
  or a negative number (like -7) to count from the right.  The number
  zero means no wrap should be done.  Most terminals have a 79- or 80-
  column display.

If your own display is wider than 80 columns, you should still wrap to less
  than 80 for the convenience of your readers on standard terminals.

For more help, type "help" and one of the following:
(at any prompt:)  save-init   set   show   send
