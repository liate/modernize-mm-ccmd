BITNET

BITNET (Because It's Time NETwork) is a cooperative network serving over
2,000 hosts worldwide.  There are four main parts: BITNET (USA, Mexico),
NetNorth (Canada), EARN (Europe, Africa) including ILAN (Israel), Asianet 
(east Asia).  For electronic mail and other practical purposes it can be 
considered one network.  

BITNET mail addresses are of the form userid@node.  The "nodes", as hosts 
are known in BITNET, have short names of 8 characters or less.  There is 
usually only one path between any two nodes, and mail is passed along 
through various intermediate nodes until it reaches the destination.  The
links in the USA are leased phone lines at 9600 baud.  Because of limited
capacity, very long messages may be held at one of the intermediate points
until overnight hours or weekends, and extremely long messages (over 300k)
may be refused.

Columbia University hosts are directly connected to two major networks,
BITNET and the Internet.  You can send and receive electronic mail easily
by either network.  Some other sites may be only on BITNET or only on the
Internet (or neither), so you may or may not have a choice of routes.
  
The nodename for cunixa, cunixb, cunixd, cunixe and cunixf is "cunixc".  
Thus, for user jb51 on cunixf, the BITNET address is jb51@cunixc.  

To send mail via BITNET, we recommend you add ".bitnet" to the address.
For example, to reach the user abcdef@cunyvm, use "abcdef@cunyvm.bitnet".
".bitnet" is not really part of the address, but rather an instruction
to the mail system about how to send the message.

For more information about BITNET, see the files in the directory
/usr/local/doc/bitnet, particularly:
  bitnet.userhelp      An introduction to BITNET.
  listserv.lists       A list of "listserv" discussion groups
  nodes.info1 and 2    BITNET nodes arranged by node (1) and site (2)

You may want to read the first two with emacs.

The nodes files are most useful with the grep command, as in
  $ grep "Fordham" /usr/local/doc/bitnet/nodes.info1
which will list the nodes at Fordham University (for example).

See also "help internet" and "help addressing".
