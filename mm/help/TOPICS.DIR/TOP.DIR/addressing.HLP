ADDRESSING

The usual way to address your mail is to type the addresses at the TO and
  CC prompts during the SEND procedure.  At both prompts, you can type more
  than one address, separated by commas.  The following forms can be used:

userid          A userid on the same host or system (cunixa to cunixf).

userid@host     A userid on the host specified.  For other hosts at Columbia,
                  the name is usually enough (cuvmb).  
                For Internet: domain name (like userid@system.university.edu)
                  or IP address (numbers) in [] brackets.                
                For BITNET: add ".bitnet" (like userid@node.bitnet)

*filename       A file in your directory.  Not recommended; use the FCC field
                  instead, or the variable SAVED-MESSAGES-FILE.

@filename       A file containing a mailing list.  The file must contain one
                  or more addresses, separated by commas.  You can insert
                  comments (explanatory text) between a # sign and the end
                  of a line, and blank lines are allowed also.
  
mail-alias      A mail-alias you created with the DEFINE command, or
                A systemwide mail-alias in the file /etc/aliases.

.               (period)  Yourself.  Use to send a copy to yourself.

"string"        A user's name as in /etc/passwd.  Not recommended.

text <address>  Any address; additional text outside the <> brackets is then
                  ignored by the mail system:  my friend <userid@host> .
                  This form is used by MM in the FROM field; your name is
                  outside the <> brackets.

(text) address  Any address; additional text in parentheses () is ignored
                  by the mail system.

In SEND MODE, you can add and remove addresses by using several header-
  field commands.  Type "help header-field" at the Send> prompt, or "help
  send-mode header-field" at any prompt.

  
Some typical examples of addresses:

jb51
	An address on the same system (cunixa to cunixf): just the user id.
jpbus@cuvmb
	An address on another host at Columbia: the user id and host name.
abc@bigvax.mich.edu
	An Internet domain-name address.  For more, type "help internet".
abc@[123.45.6.78]
        An Internet IP-number address (an alternative to domain-name).
xyz@matrix.bitnet
	A BITNET address.  For more, type "help bitnet".
foo!bar!baz@uunet.uu.net
	A UUCP address, routed via an Internet gateway.
70123.4567@compuserve.com
	A Compuserve address, routed via an Internet gateway.
"/pn=s.cobol/o=fishnet/admd=telemail/c=us/"@sprint.com
	A Sprint Mail address, routed via an Internet gateway.
ian@oxford.ac.uk
	A JANET address, England, written as "uk.ac.oxford" within the UK.

For help with addresses, send electronic mail to postmaster.
