INTERNET

The Internet is a worldwide meta-network comprising over 400 networks that
are interconnected and share a common set of protocols for electronic mail
and other data exchanges.  The number of hosts has been estimated to be as
high as 500,000.  

In the USA, the Internet is organized around the NSFNET (National Science 
Foundation Network), organized and partly funded by the US Government.  
NSFNET provides a "backbone" of 13 hubs connected by 1.5 million baud data
lines, to which the mid-level networks are connected.  

The mid-level network to which Columbia is connected is NYSERNet, funded
by universities, the State of New York, some companies, and the NSF.
NYSERNet also has 1.5 million baud data lines between its major nodes, 
which include Columbia University, and NSFNET.

Columbia hosts are also directly connected to BITNET (not part of the 
Internet).  You can send and receive electronic mail easily by either 
network.  Some other sites may be only on BITNET or only on the Internet 
(or neither), so you may or may not have a choice of routes.
  

Electronic mail addresses on the Internet are written as userid@host.
The host part is divided by dots (periods) into two or more parts.

The host part conforms to the Domain Name System.  The last part is the top 
level domain: indicating the USA origin of the system, the top level domain 
indicates the type of institution in the USA, and the country in the rest of 
the world.  Probably you will see most often "edu", educational institution, 
and "com", commercial institution.  The second-last part designates the 
individual site, and any other parts designate systems and even machine names 
at the site.  You do not need to understand the meaning of a host name, but 
if it appears to use the Domain Name System, it is probably usable as an 
Internet address.

The Internet hostname for cunixa-cunixf is <machine>.cc.columbia.edu.  Thus
for the user jb51 on cunixf (faculty-staff mail) the address is:
     jb51@cunixf.cc.columbia.edu

This is the best form in which to give your address.  It can be used in this
form from anywhere on the Internet and on some other networks, and otherwise
a usable address can be constructed from it.  
  

For some additional information on the Internet, see the files in the 
directory /usr/local/doc/internet.  

    The file interest-groups.txt describes discussion groups that
    communicate by electronic mail.  This is a long file; read it with
    emacs and use control-s to search for items of possible interest.

    The subdirectory resource-guide.txt contains a guidebook to some Internet
    resources.  It is not a general introduction to the Internet and
    is not easy to read online.

The file transfer system on the Internet is ftp.  Some sites offer "anonymous
ftp", in which anyone can connect as "anonymous" and transfer certain files.
For more information, type "man ftp" at the shell prompt.

You can log in to other hosts (if you have an account) via the network by
using telnet.  For more information, type "man telnet" at the shell prompt.

See also "help bitnet" and "help addressing".
