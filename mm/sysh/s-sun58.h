/*
 * Copyright (c) 1986, 1990 by The Trustees of Columbia University in
 * the City of New York.  Permission is granted to any individual or
 * institution to use, copy, or redistribute this software so long as it
 * is not sold for profit, provided this copyright notice is retained.
 */

#ifdef RCSID
#ifndef lint
static char *s_sun40_rcsid = "$Header: /f/src2/encore.bin/cucca/mm/tarring-it-up/RCS/s-sun40.h,v 2.1 90/10/04 18:26:05 melissa Exp $";
#endif
#endif /* RCSID */

/*
 * s-sun58.h - configuration file for sunos 5.8 (SOLARIS 8)
 */
#define MM_OS_SYSV 1	/* Ensure proper setting of top-level sys defines */

/*
 * If the system normally has the Berkeley sendmail program installed and
 * used for sending mail, define SENDMAIL to be the
 * pathname by which it is normally invoked.
 */
#ifndef SENDMAIL
#define SENDMAIL "/usr/lib/sendmail"	/* we have sendmail */
#endif

/*
 * Define SPOOL_DIRECTORY to be the name of the directory to which
 * new mail is delivered.  On System V-based systems, this is usually
 * /var/mail or /usr/mail; on older Berkeley systems, it's usually
 * /usr/spool/mail.
 */
#ifndef SPOOL_DIRECTORY
#define SPOOL_DIRECTORY "/var/mail"
#endif

/*
 * If your operating system supports filenames longer than 14 characters,
 * I.e. you are running a Berkeley UNIX derivative, define HAVE_FLEXFILENAMES.
 * If you are running some sort of operating system which supports
 * long filenames only on remote filesystems (e.g. HP-UX 6.0), don't
 * define it.
 */
#define HAVE_FLEXFILENAMES	/* we have long filenames */

/*
 * Define one or more of NEED_FCNTL, NEED_UNISTD, and NEED_SYSFILE
 * as appropriate, depending on whether or not MM has to include
 * <fcntl.h>, <unistd.h>, and/or <sys/file.h> in order to obtain
 * the symbol definitions used in the open(2), fcntl(2), flock(2),
 * lockf(2), and lseek(2) system calls.
 */
#define NEED_FCNTL		/* include <fcntl.h> */
#define NEED_UNISTD		/* include <unistd.h> */
#define NEED_SYSFILE

/*
 * You will probably need this, but you may want to undefine it if
 * another system header file includes it automatically.
 */
#define NEED_IOCTL		/* include <sys/ioctl.h> */

/*
 * MM needs to refer to definitions which may appear in either
 * <time.h> or <sys/time.h>, depending on what kind of system you have.
 * <time.h> usually contains the definition of the "tm" struct, while
 * on many BSD-based systems, <sys/time.h> will include the same information
 * in addition to the "timeval" and "timezone" structs.  Define one or
 * the other, or both, as appropriate.
 */
#define NEED_SYSTIME		/* include <sys/time.h> */
#undef NEED_TIME		/* don't include <time.h> */

/*
 * Define NEED_WAIT if your system defines "union wait" in <sys/wait.h>
 * See also HAVE_WAIT3 below.
 */
#define NEED_WAIT		/* include <sys/wait.h> */

/*
 * Define HAVE_WAIT3 if your system has the wait3() system call.
 */
#define HAVE_WAIT3		/* we have wait3() */

/*
 * Define HAVE_JOBS if your system has job control.  Currently 4.2 job control
 * is the only type supported.
 */
#define HAVE_JOBS		/* we have job control */

/*
 * Define HAVE_BSD_SIGNALS if you have the 4.2BSD-style signal handling
 * facilities, otherwise HAVE_SIGSETS for sigset_t and sigaction.
 */
#undef HAVE_BSD_SIGNALS		/* we have 4.2 signals */
#define HAVE_SIGSETS

/*
 * Define HAVE_QUOTACTL if your system has 
 * the quotactl(2) system call.  If this is defined, MM also expects to 
 * find quota.h in <ufs/quota.h> rather than <sys/quota.h>.
 * This will be ignored unless HAVE_QUOTAS is also set by config.h to
 * indicate that the system both supports quotas and wants to use them.
 */
#undef HAVE_QUOTACTL		/* we have quotactl(), not quota() */

/*
 * Define HAVE_INDEX if your system has index(3) and rindex(3) rather than
 * than strchr(3) and strrchr(3).
 */
#undef HAVE_INDEX		/* we have index & rindex */

/*
 * Define HAVE_BSTRING if you have bcopy(3), bzero(3), and bcmp(3).  These
 * are usually present on BSD-based systems, and missing on older systems
 * where MM will use memcpy(3), memset(3), and memcmp(3).
 */
#undef HAVE_BSTRING		/* we have bcopy and friends */

/*
 * Define HAVE_VFORK if your system has the vfork(2) system call.
 */
#define HAVE_VFORK		/* we have vfork() */

/*
 * Define HAVE_GETWD if you have getwd(2).  Most BSD-based systems should.
 */
#undef HAVE_GETWD		/* we have the bsd getwd() */

/*
 * Define HAVE_GETCWD if you have getcwd(3) rather than getwd(2).  This will
 * be true on most SYSV-based systems.
 */
#define HAVE_GETCWD		/* we have getcwd() */

/*
 * Define HAVE_GETHOSTNAME if you have the gethostname(2) system call, and
 * HAVE_GETHOSTBYNAME if you have get gethostbyxxxx(3) library routines.
 * This is generally true on BSD-based systems, and some SYSV systems.
 * They should be undefined if you either don't have them, or don't use
 * them (because they're broken?).
 */
#define	HAVE_GETHOSTNAME	/* we have gethostname() */
#define	HAVE_GETHOSTBYNAME	/* we have gethostbyname() */

/*
 * Define HAVE_UNAME if you don't have the gethostname(2) system call,
 * but you do have uname(2).  This is usually true on non-networked
 * SYSV systems.
 */
#undef HAVE_UNAME		/* we don't have uname() */

/*
 * Define PHOSTNAME if you have neither gethostname(2) or uname(2).
 * It's value should be a string containing a UNIX command which will
 * print your system's hostname.
 */
/* #define PHOSTNAME "uuname -l"*/	/* program that prints our hostname */

/*
 * Define HAVE_RENAME if you have the rename(2) system call, or an equivalent
 * C library routine.  This is true on later BSD releases and SVR3.
 */
#define HAVE_RENAME		/* we have rename() */

/*
 * Define one or more of the following symbols, depending on what file-
 * locking facilities are available on your system.
 * 
 * MM prefers the fcntl interface if you've got it, since lockf and flock
 * supposedly don't work on remote files when using NFS.  However, flock
 * is sometimes preferable to fcntl; old comments say "you should probably
 * avoid telling MM you have the fcntl interface or lockf() if you've got
 * flock and you use NFS, unless you really know what you're doing."
 */
#define HAVE_F_SETLK		/* we have fcntl(fd, F_SETLK, ...) */
#undef HAVE_FLOCK		/* flock locks whole files */
#undef HAVE_LOCKF		/* lockf works too */

/*
 * If your /bin/mail program uses the flock(2) system call to prevent
 * simultaneous access to files in /usr/mail or /usr/spool/mail, define
 * MAIL_USE_FLOCK.  If this is not defined, the movemail program will
 * attempt to lock files in the spool directory using lock files.
 *
 * If you're not sure whether or not to define this symbol, you might
 * be able to find the answer by consulting the sources for /bin/mail
 * on your system, or looking in the appropriate s-*.h file from the
 * GNU Emacs distribution.  If you have the strings(1) program, you
 * can try "strings /usr/spool/mail" to see if any ".lock" files exist
 * there.
 *
 * New regime: use as many mechanisms as are defined, in the following
 * order.  At least one must be defined!
 * Note that flock(2) generally does not work over NFS, but fcntl(2) does.
 */
#define MAIL_USE_SETLK		/* Solaris sendmail uses fcntl */
#undef MAIL_USE_FLOCK		/* SunOS /bin/mail uses flock(2) */
#define MAIL_USE_LOCKFILE	/* Default of "path.lock" file */
#if !defined(MAIL_USE_SETLK) && !defined(MAIL_USE_FLOCK) && !defined(MAIL_USE_LOCKFILE)
# define MAIL_USE_LOCKFILE	/* Default */
#endif

/*
 * Define HAVE_BSD_SETPGRP if your system's setpgrp() system call takes two
 * arguments.  This is generally true on later BSD releases with job control.
 * On some systems, there is both a SYSV setpgrp() call and a setpgrp2() call,
 * the latter of which takes two arguments like the BSD setpgrp() call.  If
 * that's true on your system, you should probably also add
 *	#define setpgrp setpgrp2
 * unless your system somehow figures this out automatically.
 */
#undef HAVE_BSD_SETPGRP		/* BSD setpgrp() expects two args */
#define HAVE_SETPGID		/* New regime, more standardized */

/*
 * Define HAVE_VOIDSIG if your <signal.h> defines signal(2) as
 * void (*signal())(). This seems to be true in System V Release 3
 * and SunOS 4.0.
 */
#ifndef HAVE_VOIDSIG
#define HAVE_VOIDSIG		/* void (*signal())() */
#endif /* HAVE_VOIDSIG */

/* Define HAVE_STRERROR if strerror(3) exists.
 * Otherwise code uses old hack of sys_nerr and sys_errlist.
 * Normally a nonzero __STDC__ implies strerror is there, but not
 * on SunOS where things are screwed up.
 */
#ifndef NO_STRERROR
#if __STDC__
# define HAVE_STRERROR 
#endif /* __STDC__ */
#endif /* NO_STRERROR */

/*
 * Define NEED_VFORK if we need to include <vfork.h>.
 */
#undef NEED_VFORK		/* New systems have this in <unistd.h> */

/*
 * define volatile as static if your C compiler does not support the 
 * "volatile" directive (ANSI C).
 */
#if defined(ANSIC) && !ANSIC
#define volatile static
#endif
