/*
 * Copyright (c) 1986, 2002 by The Trustees of Columbia University in
 * the City of New York.  Permission is granted to any individual or
 * institution to use, copy, or redistribute this software so long as it
 * is not sold for profit, provided this copyright notice is retained.
 */

#ifndef lint
static char *rcsid = "$Header: /f/src2/encore.bin/cucca/mm/tarring-it-up/RCS/version.c,v 2.3 90/10/04 18:26:59 melissa Exp $";
#endif

/*
 * Dump all of the rcs-id's from MM's header files.
 */

#define RCSID
#include "mm.h"
#include "parse.h"
#include "cmds.h"
#include "message.h"
#include "babyl.h"
#include "rd.h"
#include "set.h"
#include "help.h"
#undef RCSID

/*
 * Pull in the version string, major and minor release numbers, the edit
 * number, and the who-compiled-mm string, and make them available in global
 * variables.  This is ugly but it obviates the need to recompile all the
 * modules that might want to reference the edit numbers every time version.h
 * changes.
 */

#include "version.h"

char *mm_version = MM_VERSION;
char *mm_compiled = MM_COMPILED;
int mm_major_version = MM_MAJOR;
int mm_minor_version = MM_MINOR;
int mm_patch_level = MM_PATCH;
int mm_edit_number = MM_EDIT;

/*
 * Make a guess at the operating system type, so we can include that
 * info in the headers of bug reports sent with the "bug" command.
 */

#if MM_OS_LINUX
char *OStype = "Linux";
#elif MM_OS_AIX
char *OStype = "AIX";
#elif MM_OS_DECOSF
char *OStype = "DEC OSF/1";
#elif hpux
char *OStype = "hpux";
#elif defined(pyr)
char *OStype = "Pyramid";
#elif accel
char *OStype = "Accel";
#elif ultrix
char *OStype = "Ultrix";
#elif sun
char *OStype = "SunOS";
#elif MM_OS_SOLARIS
char *OStype = "Solaris";
#elif MM_OS_BSD
char *OStype = "BSD";
#elif MM_OS_SYSV
#  if SVR3
char *OStype = "SVR3";
#  elif SVR2
char *OStype = "SVR2";
#  else
char *OStype = "SYSV";
#  endif
#elif MSDOS
char *OStype = "MS-DOS";
#else
char *OStype = "unknown";
#endif
