/*
 * Copyright (c) 1986, 2002 by The Trustees of Columbia University in
 * the City of New York.  Permission is granted to any individual or
 * institution to use, copy, or redistribute this software so long as it
 * is not sold for profit, provided this copyright notice is retained.
 */

#ifdef RCSID
#ifndef lint
static char *cmds_rcsid = "$Header: /f/src2/encore.bin/cucca/mm/tarring-it-up/RCS/cmds.H,v 2.1 90/10/04 18:23:39 melissa Exp $";
#endif
#endif /* RCSID */

/*
 * cmds.h - indices into the command dispatch table
 *
 * The value of the symbols below must correspond to the offset of
 * the corresponding command in the cmd_fn[] array!
 */

/*
 * These are also indices into the help string index.  Help topics
 * which are not commands should be #def'd at the end BEFORE NUMTOPICS
 */

#define CMD_ALIAS        0               /* */
#define CMD_ANSWER       1               /* */
#define CMD_BACKTRACK    2               /* */
#define CMD_BCC          3               /* */
#define CMD_BLANK        4               /* */
#define CMD_BROWSE       5               /* */
#define CMD_BUG          6               /* */
#define CMD_CC           7               /* */
#define CMD_CD           8               /* */
#define CMD_CHECK        9               /* */
#define CMD_CONTENT_TYPE 10              /* */
#define CMD_CONTINUE     11              /* */
#define CMD_COPY         12              /* */
#define CMD_COUNT        13              /* */
#define CMD_CREATE_INIT  14              /* */
#define CMD_DAYTIME      15              /* */
#define CMD_DEBUG        16              /* */
#define CMD_MEMDEBUG     17              /* */
#define CMD_DEFINE       18              /* */
#define CMD_DELETE       19              /* */
#define CMD_DISPLAY      20              /* */
#define CMD_DOWNLOAD     21              /* */
#define CMD_ECHO         22              /* */
#define CMD_EDIT         23              /* */
#define CMD_ERASE        24              /* */
#define CMD_EXAMINE      25              /* */
#define CMD_EXIT         26              /* */
#define CMD_EXPUNGE      27              /* */
#define CMD_FCC          28              /* */
#define CMD_FINGER       29              /* */
#define CMD_FLAG         30              /* */
#define CMD_FOLLOW       31              /* */
#define CMD_FORWARD      32              /* */
#define CMD_FROM         33              /* */
#define CMD_GET          34              /* */
#define CMD_HEADERS      35              /* */
#define CMD_HELP         36              /* */
#define CMD_INSERT       37              /* */
#define CMD_IN_REPLY_TO  38              /* */
#define CMD_JUMP         39              /* */
#define CMD_KEYWORD      40              /* */
#define CMD_KILL         41              /* */
#define CMD_LIST         42              /* */
#define CMD_LITERAL      43              /* */
#define CMD_MARK         44              /* */
#define CMD_MIME_VERSION 45              /* */
#define CMD_MOVE         46              /* */
#define CMD_NEXT         47              /* */
#define CMD_PREVIOUS     48              /* */
#define CMD_PRINT        49              /* */
#define CMD_PROFILE      50              /* */
#define CMD_PUSH         51              /* */
#define CMD_PWD          52              /* */
#define CMD_QUIT         53              /* */
#define CMD_QQUIT        54              /* */
#define CMD_READ         55              /* */
#define CMD_REMAIL       56              /* */
#define CMD_REMOVE       57              /* */
#define CMD_REPLY        58              /* */
#define CMD_REPLY_TO     59              /* */
#define CMD_RESTORE_DRAFT 60             /* */
#define CMD_REVIEW       61              /* */
#define CMD_ROUTE        62              /* */
#define CMD_SAVE_DRAFT   63              /* */
#define CMD_SEND         64              /* */
#define CMD_SET          65              /* */
#define CMD_SHOW         66              /* */
#define CMD_SMAIL        67              /* */
#define CMD_SORT         68              /* */
#define CMD_SPELL        69              /* */
#define CMD_STATUS       70              /* */
#define CMD_SUBJECT      71              /* */
#define CMD_SUSPEND      72              /* */
#define CMD_TAKE         73              /* */
#define CMD_TEXT         74              /* */
#define CMD_TO           75              /* */
#define CMD_TRANSFER_ENCODING 76         /* */
#define CMD_TYPE         77              /* */
#define CMD_UNANSWER     78              /* */
#define CMD_UNDELETE     79              /* */
#define CMD_UNFLAG       80              /* */
#define CMD_UNKEYWORD    81              /* */
#define CMD_UNMARK       82              /* */
#define CMD_USER_HEADER  83              /* */
#define CMD_VERSION      84              /* */
#define CMD_WHO          85              /* */
#define CMD_WRITE        86              /* */
#define CMD_Z            87              /* */

#define CMD_HEADER       88              /* */
#define CMD_EXPAND       89              /* */

#define CMD_SENDER       90              /* */
#define CMD_ALL          91              /* */

#define CMD_INCLUDE      92              /* */
#define CMD_NOINCLUDE    93              /* */

#define HLP_CCMD         94              /* */
#define HLP_MESSAGE_SEQUENCE 95          /* */
#define HLP_MMINIT       96              /* */
#define HLP_MM           97              /* */
#define HLP_SHELL        98              /* */

#define HLP_TYPE_BABYL   99              /* */
#define HLP_TYPE_START HLP_TYPE_BABYL
#define HLP_TYPE_MBOX    100             /* */
#define HLP_TYPE_MH      101             /* */
#define HLP_TYPE_MTXT    102             /* */
#define HLP_TYPE_POP2    103             /* */
#define HLP_TYPE_POP3    104             /* */

#define NUMTOPICS        105             /* */
