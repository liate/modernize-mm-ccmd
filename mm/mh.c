/*
 * Copyright (c) 1986, 1990 by The Trustees of Columbia University in
 * the City of New York.  Permission is granted to any individual or
 * institution to use, copy, or redistribute this software so long as it
 * is not sold for profit, provided this copyright notice is retained.
 */

#ifndef lint
static char *rcsid = "$Header: /f/src2/encore.bin/cucca/mm/tarring-it-up/RCS/mh.c,v 2.1 90/10/04 18:24:46 melissa Exp $";
#endif

/* mh.c: routines to handle mh type mail files */

#include "mm.h"


mh_open (mail)
msgvec *mail;
{
    printf ("mh format not supported yet\n");
    return(false);			/* let them know we failed */
}

mh_probe (file)
char *file;
{
    return (false);
}
/* things to be done */

mh_close () {
    return(false);			/* let them know we failed */
}
mh_rdmsg () {
    return(false);			/* let them know we failed */
}
mh_wrmsg () {
    return(false);			/* let them know we failed */
}
