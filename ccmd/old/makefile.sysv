#
# Copyright (c) 1986, 1990 by The Trustees of Columbia University in
# the City of New York.  Permission is granted to any individual or
# institution to use, copy, or redistribute this software so long as it
# is not sold for profit, provided this copyright notice is retained.
#
# Author: Howie Kaye

.SUFFIXES: .c .o .obj .exe .a .lib .h .h4 .tmp

LOCAL=./local
MACHINE=SYSV
CFLAGS= -g -I. -I$(LOCAL) -D$(MACHINE)

LIBNAME=ccmd-noran.a

INCLUDES=ccmd.h cmfnc.h cmfncs.h datime.h ccmdmd.h cmkeyval.h
LOCALINCLUDES= ${LOCAL}/ccmd.h ${LOCAL}/cmfnc.h ${LOCAL}/cmfncs.h ${LOCAL}/datime.h $(LOCAL)/ccmdmd.h $(LOCAL)/cmkeyval.h

OBJ=.o
EXE=

COBJS=ccmd$(OBJ) ccmdut$(OBJ) ccmdio$(OBJ) ccmdmd$(OBJ) ccmdst$(OBJ) \
	stdact$(OBJ) cmcfm$(OBJ) cmkey$(OBJ) cmnum$(OBJ) cmqst$(OBJ) \
	cmnoi$(OBJ) cmtxt$(OBJ) cmfld$(OBJ) cmtok$(OBJ) cmswi$(OBJ) \
	cmpara$(OBJ) cmtad$(OBJ) cmmisc$(OBJ)

OBJS=ccmd$(OBJ) ccmdut$(OBJ) ccmdio$(OBJ) ccmdmd$(OBJ) ccmdst$(OBJ) \
	stdact$(OBJ) cmcfm$(OBJ) cmkey$(OBJ) cmnum$(OBJ) cmqst$(OBJ) \
	cmnoi$(OBJ) cmtxt$(OBJ) cmfld$(OBJ) cmtok$(OBJ) cmswi$(OBJ) \
	cmpara$(OBJ) cmtad$(OBJ) cmmisc$(OBJ) cmusr$(OBJ) datime$(OBJ) \
	cmfil$(OBJ) filelist$(OBJ) wild$(OBJ) cmchar$(OBJ) cmgrp$(OBJ) \
	cmver$(OBJ) 

LIBS=ccmd.a -lcurses
#LIBS=-lccmd -ltermlib

RM=rm
LINK=$(CC) $(CFLAGS) -o $@ $@${OBJ} $(LIBS)
COPY=cp
CHMOD=chmod u+w
LIBADD=ar r $(LIBNAME) $?
RANLIB=ar cr $@ `lorder $(OBJS) | tsort`
EXT=unx
SPLIT=./split$(EXE)
MKDIR=mkdir
RENAME=mv
MAKE=make

all: test$(EXE) skel$(EXE)

ccmd: ccmd.a

split$(EXE): split$(OBJ)
	$(CC) -o split split$(OBJ)

cmkeyval: cmkeyval$(OBJ)
	$(CC) -o cmkeyval cmkeyval$(OBJ)

cmkeyval.h: cmkeyval
	./cmkeyval > cmkeyval.h

cmfnc.h: cmfnc.h4 cmconf.h4 cmcfm.cnf cmkey.cnf cmnum.cnf cmfld.cnf \
	 cmqst.cnf cmtxt.cnf cmswi.cnf cmtad.cnf cmtok.cnf cmnoi.cnf \
	 cmgnrc.cnf cmfil.cnf cmusr.cnf cmgrp.cnf cmpara.cnf split$(EXE) \
	 cmfnc.top cmfncs.top cmchar.cnf cmkeyval.h
	m4 cmfnc.h4 > cmfnc.tmp
	$(SPLIT) < cmfnc.tmp
	$(RM) cmfnc.tmp

cmfncs.h: cmfnc.h

$(LOCAL): $(INCLUDES)
	-$(MKDIR) $(LOCAL)

$(LOCALINCLUDES): $(INCLUDES)
	-$(MKDIR) $(LOCAL)
	-$(COPY) $(INCLUDES) $(LOCAL)
	-$(CHMOD) $(LOCALINCLUDES)
ccmdmd.c: ccmdmd.$(EXT)
	-$(COPY) ccmdmd.$(EXT) ccmdmd.c

$(COBJS): $(LOCALINCLUDES)

cmusr$(OBJ): cmusr.c $(LOCALINCLUDES) cmusr.h

datime$(OBJ): datime.c datime.h tzone.h dtpat.h

cmfil$(OBJ): cmfil.c $(LOCALINCLUDES) cmfbrk.$(EXT) filelist.h cmfil.h 

filelist$(OBJ): filelist.c $(LOCALINCLUDES) filelist.h

wild$(OBJ): wild.c

cmchar$(OBJ): cmchar.c

cmgrp$(OBJ): cmgrp.c $(LOCALINCLUDES) cmgrp.h

cmver$(OBJ): cmver.c cmver.h

$(LIBNAME): $(OBJS)
	$(LIBADD)

#only for msdos
# dir.${OBJ}: dir.c
#	$(CC) dir.c 
#
# $(LIBNAME):: dir$(OBJ)
#	$(LIBADD) 
#end msdos only

ccmd.a:	$(LIBNAME)
	$(COPY) $(LIBNAME) ccmd.a
	$(RANLIB) 

ask$(EXE): ask.$(OBJ)
	cc -o ask ask.$(OBJ)

newversion1: ask
	@echo 'Answer no unless you wish to update version info.'
	-@if ( ask 'make a new version? ') then make newversion1 ; fi

newversion:
	co -l cmver.h
	incversion$(EXE) < cmver.h > cmver.h.new
	$(RENAME) cmver.h cmver.h.old
	$(RENAME) cmver.h.new cmver.h
	ci -u cmver.h
	$(MAKE) ccmd.a

#install: $(LOCALINCLUDES) ccmd.a newversion1
install: $(LOCALINCLUDES) ccmd.a
	$(COPY) ccmd.a /usr/local/lib/libccmd.a
	$(RANLIB) /usr/local/lib/libccmd.a

/usr/local/lib/libccmd.a: $(LOCALINCLUDES) ccmd.a
	$(MAKE) install

test$(OBJ): test.c $(LOCALINCLUDES)

test$(EXE): test$(OBJ) ccmd.a
	$(LINK)

skel$(OBJ): skel.c $(LOCALINCLUDES)

skel$(EXE): skel$(OBJ) ccmd.a
	$(LINK)

