/*
 Copyright (c) 1986, 1990 by The Trustees of Columbia University in
 the City of New York.  Permission is granted to any individual or
 institution to use, copy, or redistribute this software so long as it
 is not sold for profit, provided this copyright notice is retained.

*/

/* First determine compiler environment, if possible
 */
#ifndef __STDC__
# define ANSIC 0
#else
# define ANSIC __STDC__
#endif


/*
 * Next, include site-specific configuration information.
 * 
 */

#include "machdep.h"

/*
 * Now include the rest of the header files and add any other appropriate
 * definitions, based on what we know from the header files included so far.
 */

/*
 * Make a guess as to some os-dependent include files we'll need.
 * These #define's can be overridden in site.h.
 * KLH: None of the needXXX macros are used in any other file but this one.
 */

#if CCMD_OS_BSD
# define needSYSTIME
# define needFCNTL
# undef  needUNISTD
# define needSYSFILE
#endif

#if CCMD_OS_SYSV
# define needFCNTL
# define needUNISTD			/* you may need to override */
# undef  needSYSFILE			/*  these two in site.h */
#endif

/*
 * First look for files which can be included unconditionally on any
 * UNIX variant.
 */
#include <sys/types.h>
#include <sys/param.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <errno.h>

#ifdef needSYSTIME
#include <sys/time.h>
#else
#include <time.h>
#endif

/*
 * We need one or more of unistd.h, fcntl.h, and sys/file.h.
 */

#ifdef needFCNTL
#include <fcntl.h>
#endif

#if HAVE_UNISTD || defined(needUNISTD)
# include <unistd.h>
# undef  HAVE_UNISTD
# define HAVE_UNISTD 1
#endif

#if !defined(O_RDONLY) || !defined(R_OK)
#define needSYSFILE
#endif
#if !defined(SEEK_SET) && !defined(L_SET)
#define needSYSFILE
#endif
#ifdef needSYSFILE
#include <sys/file.h>
#endif

/*
 * CCMD routines generally assume the presence of
 * the Berkeley strings(3) and bstring(3) routines.
 */
#if !HAVE_INDEX
# define index strchr
# define rindex strrchr
#endif

#if !HAVE_BSTRING
# define bzero(a,b)	memset((a),0,b)
# define bcopy(a,b,c)	memcpy((b),(a),c)
# define bcmp(a,b,c)	memcmp((a),(b),c)
#endif


/*
 * Miscellaneous other files we need.
 */

/*#include <varargs.h>*/
#include <stdarg.h>
#include <stdlib.h>
#include <ctype.h>


/*
 * Finally, include ccmd.h, which includes stdio.h and setjmp.h.
 */

#include "ccmd.h"
