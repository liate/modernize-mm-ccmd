

				 CCMD
	   An Implementation of the TOPS-20 COMND Jsys in C

Copyright (c) 1986, 1990 by The Trustees of Columbia University in
the City of New York.

Permission is granted to any individual or institution to use, copy,
or redistribute this software so long as it is not sold for profit,
provided that this notice and the original copyright notices are
retained.  Columbia University makes no representations about the
suitability of this software for any purpose.  It is provided "as is"
without express or implied warranty.


ABSTRACT
--------

CCMD is a general parsing mechanism for developing User Interfaces to
programs.  It is based on the functionality of TOPS-20's COMND Jsys.
CCMD allows a program to parse for various field types (file names,
user names, dates and times, keywords, numbers, arbitrary text,
tokens, etc.).  It is meant to supply a homogeneous user interface
across a variety of machines and operating systems for C programs.  It
currently runs under System V UNIX, 4.2/4.3 BSD, Ultrix 1.2/2.0, and
MSDOS.  The library defines various default actions (user settable),
and allows field completion, help, file indirection, comments, etc on
a per field basis.  Future plans include command line editing, and
ports to other operating systems (such as VMS).

