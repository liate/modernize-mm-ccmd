/*
 Copyright (c) 1986, 1990 by The Trustees of Columbia University in
 the City of New York.  Permission is granted to any individual or
 institution to use, copy, or redistribute this software so long as it
 is not sold for profit, provided this copyright notice is retained.

*/
/*
 * This file contains conditional compilation directives describing the
 * presence or absence of certain common features which are not unique
 * to any one generic environment.
 *
 * Machine dependencies which need to be reflected in the compilation
 * of ccmd applications should go into ccmdmd.h.
 */

/* KLH: Completely re-written Feb-2002, along lines similar to that
 *	of the "cenv.h" file in the KLH10 distribution.
 *
 *	Note that all of the HAVE_ conditional macros must be defined as
 *	either 0 or 1 by the end of this file, and code must test their
 *	values rather than whether they are defined.
 */

/* Allow site-specific settings here to override even those provided
 * by the Makefile.
 */
#include "site.h"


/* Attempt to identify general system type being compiled for.
 * This requires conspiracy with the Makefile.
 */

/* Specific OS flags, in alpha order */

#ifndef  CCMD_OS_AIX
# define CCMD_OS_AIX 0
#endif
#ifndef  CCMD_OS_BSD	/* Old-style BSD (4.2 and earlier) */
# define CCMD_OS_BSD 0
#endif
#ifndef  CCMD_OS_BSD44	/* New-style BSD (4.4 and up) */
# define CCMD_OS_BSD44 0
#endif
#ifndef  CCMD_OS_DECOSF	/* DEC OSF/1, Digital Unix, Tru64 */
# define CCMD_OS_DECOSF 0
#endif
#ifndef  CCMD_OS_HPUX
# define CCMD_OS_HPUX 0
#endif
#ifndef  CCMD_OS_LINUX
# define CCMD_OS_LINUX 0
#endif
#ifndef  CCMD_OS_MSDOS
# define CCMD_OS_MSDOS 0
#endif
#ifndef  CCMD_OS_SOLARIS
# define CCMD_OS_SOLARIS 0
#endif
#ifndef  CCMD_OS_U3B
# define CCMD_OS_U3B 0
#endif

/* At this point we can test to see if nothing is defined, and if so
 * attempt to figure it out from well-known predefined macros.
 * What's here is the old code, which badly needs updating.
 *
 * Other macros still used in various places in the code, not
 * yet cleaned up:
 *	V7 LATTICE VENIX RAINBOW
 */
#if !(CCMD_OS_AIX|CCMD_OS_BSD|CCMD_OS_BSD44|CCMD_OS_DECOSF \
     |CCMD_OS_HPUX|CCMD_OS_LINUX|CCMD_OS_MSDOS|CCMD_OS_SOLARIS|CCMD_OS_U3B)
# ifdef unix
#  ifdef hpux
#   undef  CCMD_OS_HPUX
#   define CCMD_OS_HPUX 1
#  elif (u3b || u3b2 || u3b5 || u3b15 || u3b20)
#   undef  CCMD_OS_U3B
#   define CCMD_OS_U3B 1
#  elif /* defined(TIOCNOTTY) || */ sun || ultrix || accel
#   undef  CCMD_OS_BSD
#   define CCMD_OS_BSD 1
#  endif
# elif defined(MSDOS)
#   undef  CCMD_OS_MSDOS
#   define CCMD_OS_MSDOS 1
# endif
#endif

/* Composite flags (BSD and BSD44 probably should go here) */

#ifndef  CCMD_OS_SVR2
# define CCMD_OS_SVR2 (CCMD_OS_AIX)
#endif
#ifndef  CCMD_OS_SVR3
# define CCMD_OS_SVR3 (CCMD_OS_DECOSF|CCMD_OS_SOLARIS)
#endif
#ifndef  CCMD_OS_SVR4
# define CCMD_OS_SVR4 (0)
#endif
#ifndef  CCMD_OS_SYSV
# define CCMD_OS_SYSV (CCMD_OS_HPUX|CCMD_OS_U3B \
		      |CCMD_OS_SVR2|CCMD_OS_SVR3|CCMD_OS_SVR4)
#endif
#ifndef  CCMD_OS_UNIX
# ifdef unix
#  define CCMD_OS_UNIX 1
# else
#  define CCMD_OS_UNIX (CCMD_OS_AIX|CCMD_OS_BSD|CCMD_OS_BSD44|CCMD_OS_DECOSF \
		       |CCMD_OS_HPUX|CCMD_OS_LINUX|CCMD_OS_SOLARIS|CCMD_OS_U3B)
# endif
#endif


/* ======================================================== */

/* Weird site-dependent options, not sure where to put */

/* completion is slow when using Sun's YP facility */
/* #define NO_USERNAME_COMPLETION */
/* #define NO_GROUP_COMPLETION */


/* ======================================================== */

/* Defaults for all HAVE_ conditional macros, in quest to remove
   sys-oriented conditionals in favor of feature-oriented ones.
*/

#ifndef  HAVE_INDEX	/* System uses index/rindex? */
# define HAVE_INDEX (CCMD_OS_BSD)
#endif

#ifndef  HAVE_BSTRING	/* System has bcopy/bzero/bcmp? */
# define HAVE_BSTRING (CCMD_OS_BSD|CCMD_OS_LINUX)
#endif

#ifndef  HAVE_VOIDSIG	/* System sig handlers declared as void? */
# define HAVE_VOIDSIG (CCMD_OS_SVR3|CCMD_OS_SVR4)
#endif

#ifndef HAVE_PW_AGE
# if (CCMD_OS_BSD44|CCMD_OS_LINUX|CCMD_OS_AIX|CCMD_OS_DECOSF)
#  define HAVE_PW_AGE 0		/* Known to NOT have field */
# elif CCMD_OS_SYSV
#  define HAVE_PW_AGE 1
# else
#  define HAVE_PW_AGE 0		/* Unknown, assume 0 */ 
# endif
#endif

#ifndef HAVE_PW_COMMENT
# if (CCMD_OS_BSD44|CCMD_OS_LINUX|CCMD_OS_AIX)
#  define HAVE_PW_COMMENT 0	/* Known to NOT have field */
# else
#  define HAVE_PW_COMMENT 1
# endif
#endif


#ifndef  HAVE_TZ_SYSV	/* Have SYSV "extern timezone;" crock? */
# define HAVE_TZ_SYSV (CCMD_OS_SYSV|CCMD_OS_LINUX|CCMD_OS_SOLARIS)
#endif

#ifndef  HAVE_TZ_BSD	/* Have BSD gettimeofday() tz value? */
# define HAVE_TZ_BSD (CCMD_OS_BSD|CCMD_OS_BSD44|CCMD_OS_AIX|CCMD_OS_SVR4)
#endif

#ifndef  HAVE_VFPRINTF	/* Have ANSI vfprintf() ? */
# define HAVE_VFPRINTF (ANSIC || !CCMD_OS_BSD)
#endif

/* Determine TTY driver stuff to use.
 * HAVE_TERMIOS overrides HAVE_TERMIO which overrides HAVE_TERM_BSD
 */
#ifndef  HAVE_TERMIOS	/* System uses termios ? */
# define HAVE_TERMIOS 0
#endif
#ifndef  HAVE_TERMIO	/* System uses termio ? */
# define HAVE_TERMIO 0
#endif
#ifndef  HAVE_TERM_BSD	/* Using old BSD TTY calls? */
# define HAVE_TERM_BSD 0
#endif
#if !(HAVE_TERMIOS|HAVE_TERMIO|HAVE_TERM_BSD)
# if (CCMD_OS_SOLARIS|CCMD_OS_DECOSF|CCMD_OS_BSD44|CCMD_OS_LINUX)
#  undef  HAVE_TERMIOS
#  define HAVE_TERMIOS 1
# elif CCMD_OS_SYSV
#  undef  HAVE_TERMIO
#  define HAVE_TERMIO 1
# elif CCMD_OS_BSD
#  undef  HAVE_TERM_BSD
#  define HAVE_TERM_BSD 1
# endif
#endif

#ifndef  HAVE_UNISTD	/* System has <unistd.h> ? */
# define HAVE_UNISTD (CCMD_OS_SOLARIS|CCMD_OS_DECOSF|CCMD_OS_BSD44 \
		     |CCMD_OS_LINUX)
#endif

#ifndef  HAVE_SIGSETS	/* System has sigset_t facilities? */
# define HAVE_SIGSETS (CCMD_OS_SOLARIS|CCMD_OS_DECOSF|CCMD_OS_BSD44 \
		     |CCMD_OS_LINUX)
#endif

#ifndef  HAVE_GETENV	/* System has getenv() ? */
# define HAVE_GETENV (CCMD_OS_SOLARIS|CCMD_OS_DECOSF|CCMD_OS_BSD44 \
		     |CCMD_OS_LINUX)
#endif

#ifndef  HAVE_SETENV	/* System has setenv() ? */
# define HAVE_SETENV (CCMD_OS_DECOSF|CCMD_OS_BSD44 \
		     |CCMD_OS_LINUX)
#endif

#ifndef  HAVE_FPURGE	/* System has fpurge() ? */
# define HAVE_FPURGE (CCMD_OS_BSD44)
#endif


/*
 * We want to define exactly one of the following directory access
 * mechanisms.
 */
#ifndef  HAVE_DIRENTLIB		/* <dirent.h>  e.g. POSIX */
# define HAVE_DIRENTLIB 0
#endif
#ifndef  HAVE_DIRLIB		/* <sys/dir.h> e.g. 4.2BSD */
# define HAVE_DIRLIB 0
#endif
#ifndef  HAVE_NDIRLIB		/* <ndir.h>    e.g. HPUX */
# define HAVE_NDIRLIB 0
#endif
#ifndef  HAVE_NODIRLIB		/*  -none-     e.g. V7 */
# define HAVE_NODIRLIB 0
#endif

#if !(HAVE_DIRLIB | HAVE_NDIRLIB | HAVE_DIRENTLIB | HAVE_NODIRLIB)
# if (CCMD_OS_BSD44 | CCMD_OS_AIX | CCMD_OS_DECOSF | CCMD_OS_SOLARIS \
    | CCMD_OS_LINUX | CCMD_OS_SVR3 | CCMD_OS_SVR4)
#  undef  HAVE_DIRENTLIB
#  define HAVE_DIRENTLIB 1
# elif CCMD_OS_BSD
#  undef  HAVE_DIRLIB
#  define HAVE_DIRLIB 1
# elif CCMD_OS_HPUX
#  undef  HAVE_NDIRLIB
#  define HAVE_NDIRLIB 1
# else
#  undef  HAVE_NODIRLIB
#  define HAVE_NODIRLIB 1
# endif
#endif

