# Copyright (c) 1986, 2002 by The Trustees of Columbia University in
# the City of New York.  Permission is granted to any individual or
# institution to use, copy, or redistribute this software so long as it
# is not sold for profit, provided this copyright notice is retained.
#
# Be sure to run "make depend" before building on a new system.
# (KLH: this will not, in general, work.  Just do "make <yoursystem>".)
#
# Reasonable invocations are:
#
#	make			4.3bsd, Ultrix, Umax
#	make bsd44		4.4bsd (FreeBSD, NetBSD, OpenBSD)
#	make linux		for Linux
#	make hpux		for HP-UX systems
#	make aix		for AIX systems
#	make bsd		for 4.2bsd
#	make decosf		for DEC OSF/1 (Digital Unix, Tru64)
#	make sun		for SunOS
#	make solaris		for Sun Solaris
#	make ultrix40		for Ultrix 4.0	
#	make next		for NeXT
#	make SVR3		for SVR3
#	make SVR2		for SVR2
#	make SYSV		for older versions of System V 
#	make msdos		for MSDOS

#CC	      = ${CC}

OS	      =

DEBUGOPT      = -g

MAKEFILE      = Makefile

CFLAGS        = $(DEBUGOPT) $(OS)

OSLIBS	      =

TERMLIB	      = -ltermlib

DEST	      = /usr/local/lib

CPP	      =	/usr/ccs/lib/cpp $(DEFINES)

RANLIB	      =	ranlib

CO	      =	co

CI	      =	ci

CP	      = cp

INSTALL	      = install -c

SHELL	      = /bin/sh

LOCALINCLUDE  = ccmd.h \
		cmfnc.h \
		cmfncs.h \
		ccmdmd.h \
		datime.h

INCLUDEDIR    = /usr/include/local

M4FILES       = cmfnc.h4 \
		cmconf.h4 \
		cmfnc.top \
		cmfncs.top \
		cmcfm.cnf \
		cmchar.cnf \
		cmfil.cnf \
		cmfld.cnf \
		cmgnrc.cnf \
		cmgrp.cnf \
		cmkey.cnf \
		cmnoi.cnf \
		cmnum.cnf \
		cmpara.cnf \
		cmqst.cnf \
		cmswi.cnf \
		cmtad.cnf \
		cmtok.cnf \
		cmtxt.cnf \
		cmusr.cnf

HDRS	      =	ccmdlib.h \
		ccmd.h \
		ccmdmd.h \
		cmfil.h \
		cmfnc.h \
		cmfncs.h \
		cmgrp.h \
		cmusr.h \
		cmver.h \
		datime.h \
		dtpat.h \
		filelist.h \
		machdep.h \
		site.h \
		tzone.h

OBJS	      =	ccmd.o \
		ccmdio.o \
		ccmdmd.o \
		ccmdst.o \
		ccmdut.o \
		cmcfm.o \
		cmchar.o \
		cmfil.o \
		cmfld.o \
		cmgrp.o \
		cmkey.o \
		cmmisc.o \
		cmnoi.o \
		cmnum.o \
		cmpara.o \
		cmqst.o \
		cmswi.o \
		cmtad.o \
		cmtok.o \
		cmtxt.o \
		cmusr.o \
		cmver.o \
		cursor.o \
		datime.o \
		dir.o \
		filelist.o \
		getenv.o \
		setenv.o \
		stdact.o \
		wild.o

SRCS	      = ccmd.c \
		ccmdio.c \
		ccmdmd.unx \
		ccmdst.c \
		ccmdut.c \
		cmcfm.c \
		cmchar.c \
		cmfil.c \
		cmfld.c \
		cmgrp.c \
		cmkey.c \
		cmmisc.c \
		cmnoi.c \
		cmnum.c \
		cmpara.c \
		cmqst.c \
		cmswi.c \
		cmtad.c \
		cmtok.c \
		cmtxt.c \
		cmusr.c \
		cmver.c \
		cursor.c \
		datime.c \
		dir.c \
		filelist.c \
		getenv.c \
		setenv.c \
		stdact.c \
		wild.c \
		incversion.c \
		test.c \
		skel.c

LIBRARY	      = ccmd.a

PROGS	      = test \
		skel

ALL	      = $(LIBRARY) $(PROGS)

all:		$(ALL)

#############################################################
# OS targets
#

SVR2 SVR3 SYSV:
	$(MAKE) OS=-DCCMD_OS_$@ OSLIBS= RANLIB=: all

# FDC version - inclusion of SOLARIS seems dubious.
#SVR2 SVR3 SYSV:
#	$(MAKE) OS="-DCCMD_OS_$@ -DHAVE_DIRENTLIB -DCCMD_OS_SOLARIS" \
#		 OSLIBS= RANLIB=: all

aix:
	$(MAKE) OS=-DCCMD_OS_AIX OSLIBS= RANLIB=: all

bsd44 freebsd:
	$(MAKE) OS="-DCCMD_OS_BSD44" OSLIBS= all

netbsd openbsd:
	$(MAKE) OS="-DCCMD_OS_BSD44 -Dunix" OSLIBS= all

bsd:
	$(MAKE) $(MFLAGS) OS="-DCCMD_OS_BSD" all

decosf:
	$(MAKE) OS="-DCCMD_OS_DECOSF" OSLIBS= RANLIB=: all

linux:
	$(MAKE) OS="-DCCMD_OS_LINUX" OSLIBS= TERMLIB=-lncurses all

solaris:
	$(MAKE) CC=gcc OS="-DCCMD_OS_SOLARIS" OSLIBS= RANLIB=: all

# FDC version.  Not yet tested with KLH version.
#solaris:
#	$(MAKE) $(MFLAGS) OS="-DCCMD_OS_SOLARIS" CC=gcc all


# Old target for SunOS (and NeXT, Ultrix40) systems which used the old
# non-ANSI bundled CC rather GCC.  Kept for posterity/testing.
suncc:
	$(MAKE) $(MFLAGS) OS="-DCCMD_OS_BSD -DHAVE_VOIDSIG" all

# New default SunOS etc target, assuming GCC has been installed.
#
sun sunos next ultrix40:
	$(MAKE) $(MFLAGS) OS="-DCCMD_OS_BSD -DHAVE_VOIDSIG" CC=gcc all

hpux:
	$(MAKE) \
	OS="-DMM_OS_HPUX -DANSIC=1 +DA1.1 -Aa +O2 -Wl,-Fw -DHPUX_SOURCE" \
	OSLIBS= RANLIB=: all

msdos:
	$(MAKE) $(MFLAGS) -f makefile.dos

#############################################################
# Site-specific targets (for now)

# Panix uses NetBSD and added some mods
#	-DPANIX flushed, no longer used anywhere.
#	Doubt that -DHAVE_VFPRINTF=1 is needed now, but keep until sure.
#	Likewise -Dunix. 
panix:
	$(MAKE) OS="-DCCMD_OS_BSD44 -Dunix -DHAVE_VFPRINTF" OSLIBS= \
		DEST=/pkg/mm-0.90-20000926/lib \
		INCLUDEDIR=/pkg/mm-0.90-20000926/include \
		all


#########################################################


debug:
		$(MAKE) $(MFLAGS) DEBUGOPT=-g all


$(LIBRARY):	$(OBJS)
		ar cru $(LIBRARY) $?
		@$(RANLIB) $(LIBRARY)

test:		test.o $(LIBRARY)
		$(CC) $(CFLAGS) -o $@ $@.o $(LIBRARY) $(TERMLIB) $(OSLIBS)

skel:		skel.o $(LIBRARY)
		$(CC) $(CFLAGS) -o $@ $@.o $(LIBRARY) $(TERMLIB) $(OSLIBS)

cmfncs.h cmfnc.h: $(M4FILES) split.awk
		m4 cmfnc.h4 | awk -f split.awk

ccmdmd.c:	ccmdmd.unx
		rm -f ccmdmd.c
		cp ccmdmd.unx ccmdmd.c
		chmod u+w ccmdmd.c

clean:;		rm -f $(OBJS) $(LIBRARY) incversion test.o skel.o test skel

# KLH: Huh?  This won't work because nothing builds split.awk.
# For that matter, split.c is never used by this Makefile!
#
realclean:;	rm -f $(OBJS) $(LIBRARY) cmfncs.h cmfnc.h split.awk \
			dependencies incversion test.o skel.o test skel

checkout:;	$(CO) $(HDRS) $(SRCS) $(M4FILES) README Makefile

newversion:	incversion
		PATH=/bin:/usr/bin test -t 0
		$(CO) -l cmver.h
		./incversion < cmver.h > cmver.h.new
		mv cmver.h.new cmver.h
		$(CI) -u cmver.h

incversion:	incversion.c	
		$(CC) $(CFLAGS) -o $@ $@.c

depend:		dependencies
		rm -f junk
		sed '/^# DO NOT DELETE THIS LINE/,$$d' < $(MAKEFILE) > junk
		(echo '# DO NOT DELETE THIS LINE'; cat dependencies) >> junk
		mv junk $(MAKEFILE)
		rm dependencies

localdepend:;	rm -f junk
		sed -e '1,/^# DO NOT DELETE/!{ /:[ 	]*\//d; }' \
			< $(MAKEFILE) > junk
		mv junk $(MAKEFILE)

dependencies:	$(HDRS) site.h
		rm -f junk
		for f in $(SRCS); do $(CPP) < $$f | \
		    sed -n '/^#[ 0-9line]*"\(..*\)\".*$$/ '"s||$$f: \1|p" | \
		    sort -u ; \
		done | sed -e 's|\.c: |.o: |' -e 's|: *\./|: |' > junk
		mv junk dependencies

$(INCLUDEDIR):;
		-mkdir $(INCLUDEDIR)

install:	$(LIBRARY) $(INCLUDEDIR)
		$(INSTALL) $(LIBRARY) $(DEST)/lib$(LIBRARY)
		$(RANLIB) $(DEST)/lib$(LIBRARY)
		$(CP) $(LOCALINCLUDE) $(INCLUDEDIR)

tags:           $(HDRS) $(SRCS); ctags $(HDRS) $(SRCS)

TAGS:		$(HDRS) $(SRCS); etags $(HDRS) $(SRCS)

ccmd.tar:	$(HDRS) $(SRCS) $(M4FILES) README Makefile
		@echo 'Did you run "make localdepend"?'
		tar cf $@ README Makefile $(HDRS) $(SRCS) $(M4FILES)

update:         $(DEST)/$(LIBRARY)

$(DEST)/$(LIBRARY): $(SRCS) $(HDRS) $(EXTHDRS)
		@-ar xo $(DEST)/$(LIBRARY)
		@make -f $(MAKEFILE) DEST=$(DEST) install clean

# this line to make sure all of the headers get co'ed.
ccmd.o:		$(HDRS)

# The rest of this file contains dependencies generated with
# "make depend" -- don't add anything below or it will go away.
# DO NOT DELETE THIS LINE
